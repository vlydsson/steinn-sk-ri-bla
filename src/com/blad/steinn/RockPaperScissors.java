package com.blad.steinn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class RockPaperScissors extends Activity{
	public void onCreate(Bundle savedInstance){
		super.onCreate(savedInstance);
		setContentView(R.layout.rock_paper_scissors);
	}
	
	public void onClickRPS(View v){
		Intent i = new Intent(getApplicationContext(), ResultActivity.class);
		startActivity(i);
	}
}
