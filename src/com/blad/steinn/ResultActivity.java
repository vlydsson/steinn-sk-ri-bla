package com.blad.steinn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ResultActivity extends Activity{
	public void onCreate(Bundle savedInstance){
		super.onCreate(savedInstance);
		setContentView(R.layout.result);
		
	}
	
	public void onClickFindNew(View v){
		Intent i = new Intent(getApplicationContext(),
				FindOpponentActivity.class);
		startActivity(i);	
	}
	
	public void onClickRematch(View v){
		Intent i = new Intent(getApplicationContext(),
				RockPaperScissors.class);
		startActivity(i);	
	}
}
