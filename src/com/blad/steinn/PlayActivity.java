package com.blad.steinn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PlayActivity extends Activity{
	
	public void onCreate(Bundle savedInstance){
		super.onCreate(savedInstance);
		setContentView(R.layout.play);
	}
	
	public void onClickPlay(View v){
		Intent i = new Intent(getApplicationContext(), RockPaperScissors.class);
		startActivity(i);
	}
}
