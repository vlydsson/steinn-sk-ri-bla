package com.blad.steinn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LogInActivity extends Activity{

	
	public void onCreate(Bundle savedInstance){
		super.onCreate(savedInstance);
		setContentView(R.layout.login);
		
	}
	
	public void onClickLogIn(View v){
		Intent i = new Intent(getApplicationContext(), PlayActivity.class);
		startActivity(i);	
	}
}
