package com.blad.steinn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class FindOpponentActivity extends Activity{

	public Boolean opponentFound;
	
	public void onCreate(Bundle savedInstance){
		super.onCreate(savedInstance);
		setContentView(R.layout.findopponent);
		opponentFound = true;
		
		if(opponentFound){
			
			Intent i = new Intent(getApplicationContext(), 
					RockPaperScissors.class);
			startActivity(i);
		}
	}

	
}
